# **LEFT**

<a name="l-fw"></a>

# **1. L-FW**
```bash
echo 'l-fw.skill39.wsr' > /etc/hostname
```
- **Подключаем все диски, то есть добавлем еще 3 дисковода к машине и подключаем оставшиеся 3 диска. далее:**
```bash
# apt-cdrom add
# apt install -s frr
# apt download lib-ares2 frr frr-pythontools
# apt install frr ssh
```
## **SSH**
```bash
# apt install ssh -y
# vim /etc/ssh/sshd_config 
```
- **находим строку `#PermitRootLogin` и приводим к виду `PermitRootLogin yes`**
**в конец файла добавляем `AllowUsers root ssh_p ssh_c`**

- **перезапуск ssh**
```bash
# systemctl restart ssh
```
```bash
# adduser ssh_p 
```
- **пароль устанавливаем согласно заданию, то есть `ssh_pass`**
```bash
# adduser ssh_c
```
- **пароль устанавливаем согласно заданию, то есть `ssh_pass`**
## **IP ADDRESS**
```bash
# vim /etc/network/interfaces
```
- **приводим к виду**
```bash
auto lo ETH0 ETH1 ETH2 ETH3
iface lo inet loopback
allow-hotplug ETH0 ETH1 ETH2 ETH3
iface ETH0 inet static
    address 10.10.10.1/24
    gateway 10.10.10.10
iface ETH1 inet static
    address 172.16.20.1/24
iface ETH2 inet static
    address 172.16.50.1/30
iface ETH3 inet static
    address 172.16.55.1/30
post-up ip tunnel add tun1 mode gre local 10.10.10.1 remote 20.20.20.100 ttl 255
post-up ip address add 10.5.5.1/30 dev tun1
post-up ip link set tun1 up
```
- **перезагрузка сети**
```bash
# systemctl restart networking
```
## **FRR**
```bash
# vim /etc/frr/daemons
```
- **меняем сторку `ospfd=no` на `ospfd=yes`**
```bash
# service frr restart
добавляем строку в файл `/etc/sysctl.conf`
# echo "net.ipv4.ip_forward=1" >> /etc/sysctl.conf
либо отредактировать этот файл врчную
# sysctl -p
# vtysh
```
```bash
conf t
router ospf
passive-interface ETH0
passive-interface ETH1
network 10.5.5.0/30 area 0
network 172.16.20.0/24 area 0
network 172.16.50.0/30 area 0
network 172.16.55.0/30 area 0
default-infaormation originate
exit
exit
write
exit
```
```bash
# systemctl restart frr
```
## **IPTABLES**
```bash
# iptables -t nat -A POSTROUTING -j MASQUERADE
# iptables -t nat -A PREROUTING -p udp --dport 53 -i ETH0 -j DNAT --to-destination 172.16.20.10
# apt install iptables-persistent
# iptables-save > /etc/iptables/rules.v4
```
## **OpenVPN**
```bash
# apt install openvpn -y
# mkdir /opt/vpn
//TODO настройка OPENVPN написать
```
## **IPSec**
```bash
//TODO настройка IPSec
```

<a name="l-rtr-a"></a>

# **2.    L-RTR-A**
```bash
echo 'l-rtr-a.skill39.wsr' > /etc/hostname
```
## **IP ADDRESS**
```bash
#vim /etc/network/interfaces
```
```bash
auto lo ETH0 ETH1
iface lo inet loopback
allow-hotplug ETH0 ETH1
iface ETH0 inet static
    address 172.16.50.2/30
iface ETH1 inet static
    address 172.16.100.1/24
```
## **SSH**
```bash
# apt install ssh -y
# vim /etc/ssh/sshd_config 
```
- **находим строку `#PermitRootLogin` и приводим к виду `PermitRootLogin yes`**

- **перезапуск ssh**
```bash
# systemctl restart ssh
```
## **FRR**
```bash
# scp root@172.16.50.1:/root/*.deb ./
# dpkp -i ./*.deb
```
> Возможно данная команда не сработает
> поэтому не исключаем варианта копирования пакетов по отдельности
```bash
# vim /etc/frr/daemons
```
- **меняем сторку `ospfd=no` на `ospfd=yes`**
```bash
# service frr restart
добавляем строку в файл `/etc/sysctl.conf`
# echo "net.ipv4.ip_forward=1" >> /etc/sysctl.conf
либо отредактировать этот файл врчную
# sysctl -p
# vtysh
```
- **перезагрузка сети**
```bash
# systemctl restart networking
```
```bash
# vtysh
```
```bash
conf t
router ospf
passive-interface ETH1
network 172.16.50.0/30 area 0
network 172.16.100.0/24 area 0
exit
exit
write
exit
```
```bash
#  systemctl restart frr
```
## **DHCP**
```bash
# apt install isc-dhcp-server
# vim /etc/default/isc-dhcp-server
```
```bash
INTERFACESv4="ens192 ens224"
```
```bash
# vim /etc/dhcp/dhcpd.conf
```
```bash
default-lease-time 600;
max-lease-time 7200;

ddns-update-style interim;

update-static-leases on;

authoritative;

option domain-name "skill39.wsr";
option domain-name-servers 172.16.20.10;

zone skill39.wsr. {
    primary 172.16.20.10;
}
zone 16.172.in-addr.arpa. {
    primary 172.16.20.10;
}
zone 168.192.in-addr.arpa. {
    primary 172.16.20.10;
}

subnet 172.16.100.0 netmask 255.255.255.0 {
    range 172.16.100.65 172.16.100.75;
    option routers 172.16.100.1;
}
subnet 172.16.200.0 netmask 255.255.255.0 {
    range 172.16.200.65 172.16.200.75;
    option routers 172.16.200.1;
}
subnet 172.16.50.0 netmask 255.255.255.252 {}

host L-CLI-B {
    hardware ethernet MACADDRESS;
    fixed-address 172.16.200.61;
}
```
```bash
# systemctl restart isc-dhcp-server
```
## **LOOPBACK ADDRESS**
//TODO написать

<a name="l-rtr-b"></a>

# **3.    L-RTR-B**
```bash
echo 'l-rtr-b.skill39.wsr' > /etc/hostname
```
## **IP ADDRESS**
```bash
#vim /etc/network/interfaces
```
```bash
auto lo ETH0 ETH1
iface lo inet loopback
allow-hotplug ETH0 ETH1
iface ETH0 inet static
    address 172.16.55.2/30
iface ETH1 inet static
    address 172.16.200.1/24
```
## **SSH**
```bash
# apt install ssh -y
# vim /etc/ssh/sshd_config 
```
- **находим строку `#PermitRootLogin` и приводим к виду `PermitRootLogin yes`**

- **перезапуск ssh**
```bash
# systemctl restart ssh
```
## **FRR**
```bash
# scp root@172.16.55.1:/root/*.deb ./
# dpkp -i ./*.deb
```
> Возможно данная команда не сработает
> поэтому не исключаем варианта копирования пакетов по отдельности
```bash
# vim /etc/frr/daemons
```
- **меняем сторку `ospfd=no` на `ospfd=yes`**
```bash
# service frr restart
добавляем строку в файл `/etc/sysctl.conf`
# echo "net.ipv4.ip_forward=1" >> /etc/sysctl.conf
либо отредактировать этот файл врчную
# sysctl -p
# vtysh
```
- **перезагрузка сети**
```bash
# systemctl restart networking
```
- **настройка frr**
```bash
# vtysh
```
```bash
conf t
router ospf
passive-interface ETH1
network 172.16.55.0/30 area 0
network 172.16.200.0/24 area 0
exit
exit
write
exit
```
```bash
#  systemctl restart frr
```
## **DHCP-RELAY**
```bash
# apt install isc-dhcp-relay
```
- **При установке указываем только ip адрес `172.16.50.2`**
- **остальные пункты осталвяем пустыми**
- **Если при установке данного пакета допустили ошибку в ip**
- **то редактируем файл `/etc/default/isc-dhcp-relay`**
- **указываем только ip: `172.16.50.2`**
- **остальные строки оставляем пустыми**

<a name="l-srv"></a>

# **4.    L-SRV**
```bash
echo 'l-srv.skill39.wsr' > /etc/hostname
```
## **IP ADDRESS**
```bash
# vim /etc/network/interfaces
```
```bash
auto lo ETH0
iface lo inet loopback
allow-hotplug ETH0
iface ETH0 inet static
    address 172.16.20.10/24
    gateway 172.16.20.1
```
```bash
# systemctl restart networking
```
## **SSH**
```bash
# apt install ssh -y
# vim /etc/ssh/sshd_config 
```
- **находим строку `#PermitRootLogin` и приводим к виду `PermitRootLogin yes`**

- **перезапуск ssh**
```bash
# systemctl restart ssh
```
## **DNS**
```bash
# apt install bind9
# mkdir /opt/dns
# cp /etc/bind/db.local /opt/dns/db.skill39.wsr
# cp /etc/bind/db.127 /opt/dns/db.16.172
# cp /etc/bind/db.127 /opt/dns/db.168.192
```
- **редактирование файлов, необходимо привести к указаному виду**
```bash
# vim /etc/bind/named.conf.option
```
```bash
options {
    directory "/var/cache/bind";
    forwardes {
        10.10.10.10;
    }
    dnssec-validation no;
    allow-query {any;};
    listen-on-v6 {any;};
}
```
```bash
# vim /etc/bind/named.conf.default-zones
```
- **в этом файлк добавляем (в конец или начало, не имеет значения) следующий код:**
```bash
zone "skill39.wsr" {
    type master;
    file "/opt/dns/db.skill39.wsr";
    allow-update {any;};
    notify yes;
}
zone "16.172.in-addr-arpa" {
    type master;
    file "/opt/dns/db.16.172";
    allow-update {any;};
}
zone "168.192.in-addr-arpa" {
    type master;
    file "/opt/dns/db.168.192";
    allow-update {any;};
}
```
- **редактирование файлов конфигурации зон**
```bash
# vim /opt/dns/db.skill39.wsr
```
- **приводим к виду**

![zone](db.skill39.wsr.jpg "Прямая зона")
- **для быстрой замены в vim можно использовать следующую конструкцию**

        :%s/localhost/skill39.wsr/g

```bash
# vim /opt/dns/db.16.172
```
- **приводим к виду**

![zone](db.16.172.jpg "Прямая зона")

- **файл `/opt/dns/db.168.192` редактируем по примеру файла `/opt/dns/db.16.172`**
## [**НАСТРОЙКА LDAP**](LDAP.md)
## [**RSYSLOG**](SYSLOG.md)
## [**RSYNC**](RSYNC.MD)

<a name="l-cli-b"></a>

# **5.    L-CLI-B**
```bash
echo 'l-cli-b.skill39.wsr' > /etc/hostname
```
## **SSH**
```bash
# apt install ssh -y
# vim /etc/ssh/sshd_config 
```
## **IP ADDRESS**
- **ip получаем по dhcp**
- **для перезагрузки интерфейса можно использовать следующие команды:**
1. установить net-tools
```bash
# apt install net-tools
```
2. **перезагрузить интерфейс**
```bash
ifdown ETH0 && ifup ETH0
```

<a name="l-cli-a"></a>

# **6.    L-CLI-A**
```bash
echo 'l-cli-a.skill39.wsr' > /etc/hostname
```
## **SSH**
```bash
# apt install ssh -y
# vim /etc/ssh/sshd_config 
```
- **находим строку `#PermitRootLogin` и приводим к виду `PermitRootLogin yes`**

- **перезапуск ssh**
```bash
# systemctl restart ssh
```
## **IP ADDRESS**
- **ip получаем по dhcp**
- **для перезагрузки интерфейса можно использовать следующие команды:**
1. установить net-tools
```bash
# apt install net-tools
```
2. **перезагрузить интерфейс**
```bash
ifdown ETH0 && ifup ETH0
```

### **СКРИПТ УСТАНОВКИ ПАКТОВ, КОПИРОВАНИЯ ФАЙЛОВ И ПОДГОТОВКА К НЕМУ**
### **1.    настройка файла `/etc/hosts`**
- **добавляем в конец след строки**
```bash
172.16.20.10    l-srv
172.16.50.2    l-rtr-a
172.16.55.2    l-rtr-b
10.10.10.1    l-fw
20.20.20.100    r-fw
192.168.20.10    r-srv
192.168.10.2    r-rtr
192.168.100.100    r-cli
20.20.20.5    out-cli
```
- **можно писать не весь список**
### **2.    файл `/etc/resolv.conf`**
```bash
domain skill39.wsr
search skill39.wsr
nameserver 172.16.20.10
```
### **3.    файл `/etc/nsswitch.conf`**
        здесь необзодимо найти строку
        host    files dns
        и привести к виду
        host    dns files
### **4. скрипт**
**нужно создать два файла**

        hosts
        start.sh
#### **a) hosts**
        172.16.20.10
        172.16.50.1
        172.16.100.1
        172.16.55.2
        172.16.200.61

<a name="script"></a>

#### **СКРИПТ**
```bash
#!/bin/bash
for host in $(cat hosts)
do
    for file in hosts resolv.conf nsswitch.conf
    do
        sshpass -p toor scp -o StrictHostKeyChecking=no /etc/$file root@$host:/etc/$file
    done
    sshpass -p toor ssh -o StrictHostKeyChecking=no -n root@$host apt install tcpdump net-tools curl vim lynx bind9utils nfs-common cifs-utils sshpass -y
done
```