# **НАСТОРЙКА OPENVPN и ГЕНЕРАЦИЯ КЛЮЧЕЙ**


## **1. установливаем openvpn на L-FW**
```bash
# apt install openvpn -y
```
## **2. копируем пект `easy-rsa` на R-FW**
```bash
# scp -r /usr/share/easy-rsa root@20.20.20.5:/root/
```
## **3. на R-FW генерируем сертификаты и ключи**
```bash
# cd /root/easy-rsa
# ./easy-rsa init-pki
# ./easy-rsa --dn-mode=org build-ca
    при выполнении этой команды обращаем внимание на свои ответы.
    Отвечаем согласно заданию
    Country="RU"
    ORGANIZATION="WorldSkills Russia"
    CN="WSR CA"
    если ставите пароль, то он должен ббыть не менне 4 символов
    также можно создать файл `vars`  с содержимым:
    export KEY_COUNTRY="RU"
    export KEY_ORG="WorldSkills Russia"
    export KEY_CN="WSR CA"

# ./easy-rsa gen-dh
# ./easy-rsa gen-req server nopass
# ./easy-rsa sign-req server server
# ./easy-rsa gen-req client nopass
# ./easy-rsa sign-req client client
```
## **4. Копируем полученные файлы на OUT-CLI и L-FW**
```bash
на OUT-CLI сертификаты клиента
на L-FW сертификаты сервера
на оба сертифкат УЦ
# cd /root/easy-rsa
# scp pki/ca.crt pki/issued/server.crt pki/private/server.key pki/dh.pem root@l-fw:/etc/openvpn/
# scp pki/ca.crt pki/issued/client.crt pki/private/client.key root@20.20.20.5:/root/
```
## **5. на L-FW настройка конфигурации сервера**
```bash
# cd /etc/openvpn
# openvpn --genkey --secret ta.key
# zcat /usr/share/doc/openvpn/examples/simple-config-files/server.conf.gz > server.conf
# vim server.conf
    далее находим нужные строки и приводим к нужному виду
port 1195
proto udp
dev tun
ca ca.crt
cert server.crt
key server.key
dh dh.pem
server 5.5.5.0 255.255.255.224
push "route 172.16.0.0 255.255.0.0"
push "route 192.168.0.0 255.255.0.0"
push "dhcp-option DNS 172.16.20.10"
keepalive 10 120
tls-auth ta.key 0
cipher AES-256-CBC
user nobody
group nogroup
persist-key
persist-tun
status /var/log/openvpn/openvpn.log
verb 3
```

- запускаем сервис
```bash
systemctl  enable --now openvpn@server
```
## **6. Копируем ta.key и пакеты openvpn на OUT-CLI**
```bash
# scp /etc/openvpn/ta.key root@20.20.20.5:/root/
# scp /root/add/* root@20.20.20.5:/root/
```
## **7. на OUT-CLI**
### **ПОДГОТОВКА YUM**
- **после копирования подключаем  образ `CentOS-7-x86_64-Everything-1810.iso` к виртуальной машине вместо `Additional RPMs.iso`**
 

1. - далее в папке `/etc/yum.repos.d/` удаляем все файлы кроме `CentOS-Media.repo`
1. - редактируем этот файл, меняем строки 
   - `gpgcheck=1` на `gpgcheck=0`
   - `enabled=0` на `enabled=1`
1. - выполняем команду монтирования:
```bash
# mount /dev/sr0 /media/cdrom
```
- **теперь можно устанавливать пакеты**
```bash
выключаем firewalld и SELinux
# systemctl disable --now firewalld
# vim /etc/selinux/config
    меняем enforsing на disabled
```
- установка openvpn
```bash
# cd /root/
# yum install ./pkcs11-helper-1.11-3.el.x86_64.rpm 
# yum install ./openvpn-2.3.10-1.e17.x86_64.rpm
# cp ca.crt client.crt client.key ta.key /etc/openvpn/
# cp /usr/share/doc/openvpn-2.3.10/sample/sample-config-files/static-home.conf /etc/openvpn/client.conf

данный файл приводим к виду
# vim /etc/openvpn/client.conf

client
dev tun
proto udp
remote 10.10.10.1 1195
resolv-retry infinite
nobind
persist-key
persist-tun
remote-cert-tls server
verb 3
ca ca.crt
cert client.crt
key client.key
tls-auth ta.key 1
cipher AES-256-CBC
```
## **8. скрипт запуска на OUT-CLI**
start_vpn.sh
```bash
systemctl start openvpn@client
```
stop_vpn.sh
```bash
systemctl stop openvpn@client
```

# **Для установления доверия к CA выполняем следующее**

***DEBIAN***

копируем сертификат CA в папку `/usr/local/share/ca-certificates`
если папки не существует создаем ее

```bash
# cp CA.crt /usr/local/share/ca-certificates
```
далее выполнить команду 

```bash
# update-ca-certificates
```

***CENTOS***
Влючаем динамическое обновление сертификатов:
```bash
#update-ca-trust enable
```

Потом копируем нужный сертификат в доверенные:
```bash
#cp mycertfile.pem /etc/pki/ca-trust/source/anchors/
```
Обновляем доверенные сертификаты:
```bash
#update-ca-trust extract
```