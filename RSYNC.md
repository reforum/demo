# **НАСТРОЙКА RSYNC**
# **НАСТРОЙКА СЕРВЕРА L-SRV**

- 1. устанока rsync
```bash
# apt install rsync
```
- 2. создание папок и пользователя
```bash
# mkdir /opt/sync
# useradd -d /opt/sync mrsync
# chown -R mrsync. /opt/sync
```
- 3.   настойка
    - создать файл `/etc/rsyncd.conf` со следующим содержимым
```bash
# vim /etc/rsyncd.conf
```
```bash
[sync]
path = /opt/sync
hosts allow = l-cli-a l-cli-b
hosts deny = *
list = true
uid = mrsync
gid = mrsync
read only = false
```
- 4. создать файл `/etc/rsyncd.scrt`
```bash
# vim /etc/rsyncd.scrt
```
```bash
sync:parol666
```

# **КЛИЕНТЫ: L-CLI-A L-CLI-B**

- 1. устанока rsync
```bash
# apt install rsync
```
- 2. содать папку
```bash
mkdir /root/sync
```
- 3. создать скрипт запуска
```bash
# vim /root/sync.sh
```
```bash
#!/bin/bash
RSYNC_PASSWORD=parol666 rsync -avv sync@l-srv::sync /root/sync/
```
- 4. сделать испольняемым и выполнение каждую минуту
```bash
# chmod +x /root/sync.sh
# echo "*/1 * * * * root /root/sync.sh" >> /etc/crontab
```
- **выполнение каждую минуту можно сделать также через команду `crontab -e`**