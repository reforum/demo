# **НАСТРОЙКА SYSLOG**
## **L-SRV**
- **создать папки и дать права**
```bash
# mkdir -p /opt/logs/L-SRV/
# mkdir -p /opt/logs/L-FW/
# chmod 777 -R /opt/
```
- **насторйка L-SRV**
```bash
# vim /etc/rsyslog.conf
```
- **привести к виду**
- Раздел MODULES
- 
-![rsyslog](rsyslog1.PNG "rsyslog") 

- Раздел RULES 
- 
![rsyslog](rsyslog.PNG "rsyslog")

- **на L-FW**
- 
![rsyslog](rsyslog_client.PNG "rsyslog")