# Инструкция по настройке LDAP авторизации через Gosa


## 1. Настройка LDAP
- Необходимо установить пакеты openLDAP gosa
```bash
#apt install slapd gosa gosa-schema
```
- Далее необходимо отключить службу openLDAP
```
systemctl stop slapd
```
- Делаем копию схем LDAP (если вдруг пойдет что-то не так) и очищаем каталоги
```bash
#cp /etc/ldap/slapd.d/ /etc/ldap/slapd1.d/
#rm -rf /etc/ldap/slapd.d/
#rm -rf /var/lib/ldap
```
- Теперь переконфигурируем openLDAP
 ```bash
 #dpkg-reconfigure slapd
 ```
 отвечаем на вопросы согласно заданию, на последние два вопроса ответ "да"

## 2. Настройка gosa
- Создаём файл file_name.conf 

  *где file_name, ваше название файла*
```bash
#nano /etc/ldap/convert.conf
```
- Вписываем туда следующее:
```bash
include /etc/ldap/schema/core.schema
include /etc/ldap/schema/cosine.schema
include /etc/ldap/schema/nis.schema
include /etc/ldap/schema/inetorgperson.schema
include /etc/ldap/schema/gosa/samba3.schema
include /etc/ldap/schema/gosa/gofon.schema
include /etc/ldap/schema/gosa/gosystem.schema
include /etc/ldap/schema/gosa/goto.schema
include /etc/ldap/schema/gosa/gosa-samba3.schema
include /etc/ldap/schema/gosa/gofax.schema
include /etc/ldap/schema/gosa/goserver.schema
include /etc/ldap/schema/gosa/goto-mime.schema
include /etc/ldap/schema/gosa/trust.schema
```
### ОБЯЗЯТЕЛЬНО В ТАКОМ ПОРЯДКЕ ИНАЧЕ НИЧЕГО РАБОТАТЬ НЕ БУДЕТ!!!
- Создаем директорию
```bash
#mkdir /tmp/convert_out
```
- Далее запускаем инструмент преобразования 
```bash
#slaptest -f convert.conf -F /tmp/convert_out
```
- Должно появится *config file testing succeeded*

- Далее копируем схемы в конфигурационную папку openLDAP и выдаем ему права
```bash
cp -r /tmp/convert_out/cn\=config/cn\=schema* /etc/ldap/slapd.d/cn\=config/
chown -R openldap /etc/ldap/slapd.d/
```
- проверяем схемы в папке /etc/ldap/slapd.d/cn\=config/cn\=schema

     *Должно быть от 0 до 13*

## 3. GOSA 
!! ПЕРЕД GOSA НУЖНО ОБЯЗАТЕЛЬНО НАСТРОИТЬ SSH

- Заходим на L-CLI-A 
- Открываем браузер и терминал
- Переходим по ссылке
```bash
172.16.20.10/gosa либо l-srv.skill39.wsr/gosa
```
У вас должна выйти следующее 
![GOSA](005.jpg "ldap")
*у вас будет свой ключ*
- Копируем данную команду
- Подключаемся по ssh к L-srv 
```bash
ssh root@172.16.20.10
```  
- Вставляем скопированную команду в терминал нажав на СКМ
- Доходим до данного пункта настроек
![GOSA](009.jpg "gosa")
-В строчке Base у вас должно быть следующее
```bash
dc=skill39,dc=wsr
```
-В строчке Admin DN вводим следующее:
```bash
cn=admin,dc=skill39,dc=wsr
```
- В строчке Admin password пароль который вы задали при реконфигурации
- Нажимаем Check Again

*Красная надпись должна стать зеленой*

**В следующем пункте у вас должно быть Schema check succeeded, если нет, значит вы неправильно подключили схемы**
- Далее gosa предлагает нам установить файл gosa.conf на сервер
```bash
scp /etc/downloads/gosa.conf root@172.16.20.10:/root/
```
### Поздравляю вы зашли на GUI версию настройки LDAP
- Теперь создаем пользователей и группы
- Во вкладке USERS нажимаем create>new user
 ```
First, Last name и Login пишем tux
```
- Во вкладке POSIX нажимаем на кнопку add POSIX settings
```
Прописываем пользователю UID И GID 
```
- Нажимаем OK
- Задаем пароль, согласно заданию toor
-----
- Во вкладке groups создаем 2 группы users и admin

## 4. НАСТРОЙКА КЛИЕНТОВ
- Устанавливаем следующие пакеты
```bash
#apt-get install libnss-ldap libpam-ldap nscd libnss-ldapd
```
И следуем подсказкам псевдографической оболочки
- Выходим из учетной записи и заходим под пользователем tux

# Поздравляю вы настроили LDAP

