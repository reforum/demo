# ***Решение Модуль А LINUX***
- ## **вместо редактора vim  можете использовать любой другой. Но помните, что его сначала нужно установить.**

- # [**LEFT**](LEFT.md#l-fw)
1. [**L-FW**](LEFT.md#l-fw)
1. [**L-RTR-A**](LEFT.md#l-rtr-a)
1. [**L-RTR-B**](LEFT.md#l-rtr-b)
1. [**L-SRV**](LEFT.md#l-srv)
1. [**L-CLI-B**](LEFT.md#l-cli-b)
1. [**L-CLI-A**](LEFT.md#l-cli-a)
1. [**script**](LEFT.md#script)
1. [**LDAP**](LDAP.md#script)
1. [**SYSLOG**](SYSLOG.md#script)
1. [**RSYNC**](RSYNC.md#script)

- # [**RIGHT**](RIGHT.MD)
1. [**R-FW**](RIGHT.md#r-fw)
1. [**R-RTR**](RIGHT.md#r-rtr)
1. [**R-SRV**](RIGHT.md#r-srv)
1. [**R-CLI**](RIGHT.md#r-cli)
1. [**APACHE**](RIGHT.md#apache)