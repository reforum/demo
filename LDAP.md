# **НАСТРОЙКА LDAP**
 **произвадиться на L-SRV**

## **1. Добавление групп**
- Необходимо добавить группы в ОС
- Создаем группу Admin
```bash
groupadd Admin
```
- точно так же группу Guest
```bash
groupadd Guest
```

## **2. Добавляем пользователей в систему**
- один польщователь tux
```bash
useradd -g Admin tux
useradd -g Guest user
```
- также назначаете пользователю пароль
```bash
passwd tux
passwd user
```
- пароль согласно заданию для tux toor
- для user P@ssw0rd

## **3. Насторйка LDAP**
```bash
# apt install slapb ldaputils migrationtools
```
```bash
# systemctl stop slapd
```
```bash
# dpkg-reconfigure slapd
```
- **отвечаем на вопросы согласно заданию, на последние два вопроса ответ "да"**

- переходим в папку `/usr/share/migrationtools`
- редактируем файл `migrate_common.ph`:
```perl
$DEFAULT_MAIL_DOMAIN = "skill39.wsr";

$DEFAULT_BASE = "dc=skill39,dc=wsr";
```
1. 
```bash
# cp /usr/share/migrationtools/migrate_common.ph /etc/perl/
```
2. выполняем:
```bash
# cd /usr/share/migrationtools
# migrate_base.pl > ~/base.ldif
# migrate_group.pl /etc/group ~/groups.ldif
# migrate_passwd.pl /etc/passwd ~/passwd.ldif
```
- **при выполнении данных команд создаются файлы со списоком существующих пользователей, можно оставить, можно оставить в файле только те что нужно**
- **подгруужаем данные в LDAP базу**
```bash
# ldapadd -x -W -D cn=admin,dc=skill39,dc=wsr -f ~/base.ldif
# ldapadd -x -W -D cn=admin,dc=skill39,dc=wsr -f ~/groups.ldif
# ldapadd -x -W -D cn=admin,dc=skill39,dc=wsr -f ~/passwd.ldif
```

## **4. НАСТРОЙКА КЛИЕНТОВ**
```bash
apt-get install libnss-ldap libpam-ldap nscd -y
```
//TODO дописать

